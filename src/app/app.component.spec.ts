import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { BrowserModule } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { environment } from 'src/environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthService } from './service/auth.service';
import { GoogleLoginProvider } from 'angularx-social-login';
import { FormBuilder} from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { SocialLoginModule, SocialAuthServiceConfig } from 'angularx-social-login';



describe('AppComponent', () => {
  let app: AppComponent;
  beforeEach(async () => {
    await TestBed.configureTestingModule({ 
      imports: [
        RouterTestingModule,
        BrowserModule,
        AppRoutingModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        AngularFireMessagingModule,
        AngularFireStorageModule,
        HttpClientModule,
        SocialLoginModule,
        ReactiveFormsModule,
      ],
      providers: [
        AuthService,
        FormBuilder,
        {
          provide: 'SocialAuthServiceConfig',
          useValue: {
            autoLogin: false,
            providers: [
              {
                id: GoogleLoginProvider.PROVIDER_ID,
                provider: new GoogleLoginProvider(
                  '229836909682-aujrdh1ccu14gkc0ts7hvkagr9629urv.apps.googleusercontent.com'
                )
              }
            ]
          } as SocialAuthServiceConfig,
        }],
      declarations: [AppComponent],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;
    expect(app).toBeTruthy();

  });

  it('Should get data from API', async () => {
    const fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;
    const res = await app.getData2();
    console.log("****** Ici c'est le log de res ******"+res);
    expect(res).toEqual({
      img: "https://img.icons8.com/color/452/naruto.png",
      Nom: "Naruto Uzumaki",
      caracteristics: "Kyubi"
    });
  });
});
